<header class="header">
<div class="header-top wrap">
    <div class="header-logo">
        <?php if ($logo): ?>
            <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" class="header__logo"><img
                    src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" class="header__logo-image"/></a>
        <?php endif; ?>
    </div>
    <div class="header-menu"><?php print render($page['header_top']); ?></div>
</div>


    <?php print render($page['header']); ?>

</header>

<?php print render($page['highlighted']); ?>
<?php print $breadcrumb; ?>
<?php print render($title_prefix); ?>
<?php if ($title): ?>
    <h1><?php print $title; ?></h1>
<?php endif; ?>
<?php print render($title_suffix); ?>
<?php print $messages; ?>
<?php print render($tabs); ?>
<?php print render($page['help']); ?>
<?php if ($action_links): ?>
    <ul class="action-links"><?php print render($action_links); ?></ul>
<?php endif; ?>
<?php print render($page['content']); ?>
<?php print $feed_icons; ?>

<div class="footer">
    <div class="wrap">
        <div class="footer-logo">
            <a href="<?php print $front_page; ?>"><img
                    src="<?php print base_path() . path_to_theme() . '/logo-footer.png'; ?>"
                    alt="<?php print t('Home'); ?>"/></a>
        </div>
        <?php print render($page['footer_first']); ?>
        <?php print render($page['footer_second']); ?>
        <?php print render($page['footer_third']); ?>
        <?php print render($page['footer_last']); ?>
    </div>
</div>
<?php print render($page['bottom']); ?>
